﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupportDesk.Objects;

namespace SupportDesk.Models.Dashboard
{
	public class DashboardIndexViewModel
	{
		public List<TicketListItem> TicketListItems { get; set; }
	}
}
