﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupportDesk.Models.UserControls
{
	public class DataTableViewModel
	{
		public string Id { get; set; }

		public IEnumerable<object> Items { get; set; }
	}
}
