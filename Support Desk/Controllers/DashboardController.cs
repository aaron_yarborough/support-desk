﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SupportDesk.Objects;
using SupportDesk.Data;
using SupportDesk.Models;

namespace SupportDesk.Controllers
{
    public class DashboardController : Controller
    {
		private SupportDeskServiceReference.TicketsClient _ticketsClient = new SupportDeskServiceReference.TicketsClient();

		// GET: Dashboard
		public ActionResult Index()
        {
			List<TicketListItem> listItems = (List<TicketListItem>)_ticketsClient.GetTicketList();

			var vm = new Models.Dashboard.DashboardIndexViewModel
			{
				TicketListItems = listItems
			};

			return View(vm);
        }
    }
}