﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupportDesk.Objects;
using System.Data.SqlClient;
using System.Data;
using SupportDesk.Models;

namespace SupportDesk.Data
{
	public class TicketData : DataBase
	{
		public Ticket GetTicket(string ticketId)
		{
			var dt = new DataTable();
			using (var con = new SqlConnection(DataBase.ConnectionString))
			using (var cmd = new SqlCommand("TICKET_GET", con) { CommandType = CommandType.StoredProcedure })
			{
				con.Open();
				SqlDataAdapter da = new SqlDataAdapter(cmd);
				da.Fill(dt);
				con.Close();
			}

			if (dt.Rows.Count > 0)
			{
				var row = dt.Rows[0];
				var ticket = new Ticket
				{
					Id = (Guid)row["ID"],
					Title = (string)row["TITLE"],
					CreatorId = (Guid)row["CREATOR_ID"],
					CreateDate = (DateTime)row["CREATION_DATE"],
					UpdateDate = (DateTime)row["UPDATE_DATE"],
					Summary = (string)row["SUMMARY"],
					CategoryId = (Guid)row["CATEGORY_ID"],
					Urgency = (int)row["URGENCY"],
					HolderId = (Guid)row["HOLDER_ID"],
					Status = (int)row["STATUS"],
					DueDate = (DateTime)row["DUE_DATE"]
				};

				return ticket;
			}

			return null;
		}

		public List<TicketListItem> GetTicketListEntries()
		{
			var dt = new DataTable();
			using (var con = new SqlConnection(DataBase.ConnectionString))
			using (var cmd = new SqlCommand("TICKETLIST_GET", con) { CommandType = CommandType.StoredProcedure })
			{
				con.Open();
				SqlDataAdapter da = new SqlDataAdapter(cmd);
				da.Fill(dt);
				con.Close();
			}

			if (dt.Rows.Count > 0)
			{
				List<TicketListItem> ticketEntries = new List<TicketListItem>();
				foreach (DataRow row in dt.Rows)
				{
					//tickets.Add(new Ticket
					//{
					//	Id				= (Guid)row["ID"],
					//	Title			= (string)row["TITLE"],
					//	CreatorId		= (Guid)row["CREATOR_ID"],
					//	CreateDate		= (DateTime)row["CREATION_DATE"],
					//	UpdateDate		= (DateTime)row["UPDATE_DATE"],
					//	Summary			= (string)row["SUMMARY"],
					//	CategoryId		= (Guid)row["CATEGORY_ID"],
					//	Urgency			= (int)row["URGENCY"],
					//	HolderId		= (Guid)row["HOLDER_ID"],
					//	Status			= (int)row["STATUS"],
					//	DueDate			= (DateTime)row["DUE_DATE"]
					//});

					//var ticket = new Ticket();
					//ticket.Id = (Guid)row["ID"];
					//ticket.Title = (string)row["TITLE"];
					//ticket.CreatorId = (Guid)row["CREATOR_ID"];
					//ticket.CreateDate = (DateTime)row["CREATION_DATE"];
					//ticket.UpdateDate = null;
					//ticket.Summary = (string)row["SUMMARY"];
					//ticket.CategoryId = row["CATEGORY_ID"] == DBNull.Value ? null : (Guid?)row["CATEGORY_ID"];
					//ticket.Urgency = (int)row["URGENCY"];
					//ticket.HolderId = row["HOLDER_ID"] == DBNull.Value ? null : (Guid?)row["HOLDER_ID"];
					//ticket.Status = (int)row["STATUS"];
					//ticket.DueDate = (DateTime)row["DUE_DATE"];

					var ticketListItem = new TicketListItem();
					ticketListItem.Id = (Guid)row["ID"];
					ticketListItem.CategoryName = (string)row["CATEGORY_NAME"];
					ticketListItem.CategoryId = row["CATEGORY_ID"] == DBNull.Value ? null : (Guid?)row["CATEGORY_ID"];
					ticketListItem.Title = (string)row["TITLE"];
					ticketListItem.CreatorName = (string)row["CREATOR_NAME"];
					ticketListItem.CreateDate = (DateTime)row["CREATION_DATE"];
					ticketListItem.DueDate = (DateTime)row["DUE_DATE"];

					ticketEntries.Add(ticketListItem);

				}

				return ticketEntries;
			}

			return null;
		}
	}
}
