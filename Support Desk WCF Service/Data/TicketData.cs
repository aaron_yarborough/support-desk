﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupportDesk.Objects;
using System.Data.SqlClient;
using System.Data;

namespace SupportDesk.Service.Data
{
	public class TicketData : DataBase
	{
		public Ticket GetTicketById(Guid ticketId)
		{
			var dt = new DataTable();
			using (var con = GetConnection())
			using (var cmd = new SqlCommand("TICKET_GET", con) { CommandType = CommandType.StoredProcedure })
			{
				cmd.Parameters.AddWithValue("@Id", ticketId);

				con.Open();
				SqlDataAdapter da = new SqlDataAdapter(cmd);
				da.Fill(dt);
				con.Close();
			}

			if (dt.Rows.Count > 0)
			{
				var row = dt.Rows[0];
				var ticket = new Ticket
				{
					Id = (Guid)row["ID"],
					Title = (string)row["TITLE"],
					CreatorId = (Guid)row["CREATOR_ID"],
					CreateDate = (DateTime)row["CREATION_DATE"],
					UpdateDate = (DateTime)row["UPDATE_DATE"],
					Summary = (string)row["SUMMARY"],
					CategoryId = (Guid)row["CATEGORY_ID"],
					Urgency = (int)row["URGENCY"],
					HolderId = (Guid)row["HOLDER_ID"],
					Status = (int)row["STATUS"],
					DueDate = (DateTime)row["DUE_DATE"]
				};

				return ticket;
			}

			return null;
		}

		public List<TicketListItem> GetTicketListEntries()
		{
			var dt = new DataTable();
			using (var con = GetConnection())
			using (var cmd = new SqlCommand("TICKETLIST_GET", con) { CommandType = CommandType.StoredProcedure })
			{
				con.Open();
				SqlDataAdapter da = new SqlDataAdapter(cmd);
				da.Fill(dt);
				con.Close();
			}

			if (dt.Rows.Count > 0)
			{
				List<TicketListItem> ticketEntries = new List<TicketListItem>();
				foreach (DataRow row in dt.Rows)
				{
					var ticketListItem = new TicketListItem();
					ticketListItem.Id = (Guid)row["ID"];
					ticketListItem.CategoryName = (string)row["CATEGORY_NAME"];
					ticketListItem.CategoryId = row["CATEGORY_ID"] == DBNull.Value ? null : (Guid?)row["CATEGORY_ID"];
					ticketListItem.Title = (string)row["TITLE"];
					ticketListItem.CreatorName = (string)row["CREATOR_NAME"];
					ticketListItem.CreateDate = (DateTime)row["CREATION_DATE"];
					ticketListItem.DueDate = (DateTime)row["DUE_DATE"];

					ticketEntries.Add(ticketListItem);
				}

				return ticketEntries;
			}

			return null;
		}

		public void CreateTicket(
			string title,
			Guid creatorId, 
			string summary,
			string categoryId, 
			int urgency,
			Guid holderId, 
			DateTime dueDate)
		{
			using (var con = GetConnection())
			using (var cmd = new SqlCommand("TICKET_CREATE", con) { CommandType = CommandType.StoredProcedure })
			{
				cmd.Parameters.AddWithValue("@Title", title);
				cmd.Parameters.AddWithValue("@CreatorId", creatorId);
				cmd.Parameters.AddWithValue("@Summary", summary);
				cmd.Parameters.AddWithValue("@CaregoryId", categoryId);
				cmd.Parameters.AddWithValue("@Urgency", urgency);
				cmd.Parameters.AddWithValue("@HolderId", holderId);
				cmd.Parameters.AddWithValue("@DueDate", dueDate);

				con.Open();

				cmd.ExecuteNonQuery();

				con.Close();
			}
		}
	}
}
