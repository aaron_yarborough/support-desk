﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using SupportDesk.Objects;
using System.Runtime.Serialization;

namespace SupportDesk.Service
{
	[ServiceContract]
	public interface ITickets
	{
		[OperationContract]
		Ticket GetTicketById(Guid id);

		[OperationContract]
		List<TicketListItem> GetTicketList();

		[OperationContract]
		void CreateTicket(
			string title,
			Guid creatorId,
			string summary,
			string categoryId,
			int urgency,
			Guid holderId,
			DateTime dueDate);
	}
}
