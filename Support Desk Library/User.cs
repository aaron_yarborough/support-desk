﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupportDesk.Objects
{
	public class User
	{
		public Guid Id { get; set; }

		public string Forename { get; set; }

		public string Surname { get; set; }

		public string Email { get; set; }

		public DateTime CreateDate { get; set; }

		public string EncryptedPassword { get; set; }
	}
}
