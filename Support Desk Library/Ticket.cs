﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupportDesk.Objects
{
    public class Ticket
    {
		public Guid Id { get; set; }

		public string Title { get; set; }

		public Guid CreatorId { get; set; }

		public DateTime CreateDate { get; set; }

		public DateTime? UpdateDate { get; set; }

		public string Summary { get; set; }

		public Guid? CategoryId { get; set; }

		public int? Urgency { get; set; }

		public Guid? HolderId { get; set; }

		public int Status { get; set; }

		public DateTime DueDate { get; set; }
    }
}
