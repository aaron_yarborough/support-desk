﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupportDesk.Objects
{
	public class TicketListItem
	{
		public Guid Id { get; set; }

		public string CategoryName { get; set; }

		public Guid? CategoryId { get; set; }

		public string Title { get; set; }

		public string CreatorName { get; set; }

		public DateTime CreateDate { get; set; }

		public DateTime DueDate { get; set; }
	}
}
